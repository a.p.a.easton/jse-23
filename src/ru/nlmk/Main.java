package ru.nlmk;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ToDo toDo = new ToDo();
        List<Object> list = new ArrayList<>();
        list.add(new Person("Petr", "Petrov", "qwe@.gmail.ru" ));
        list.add(new Person("Ivan", "Ivanov", "rty@mail.ru"));
        for (List<Description> lizt : toDo.getList(list)) {
            System.out.println(lizt.toString());
        }

    }
}

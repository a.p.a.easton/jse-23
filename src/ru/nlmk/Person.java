package ru.nlmk;

import java.time.LocalDate;

public class Person {

    private  String firstName;

    private String lastName;

    private LocalDate birthDate;

    private String email;

    public Person(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }
}

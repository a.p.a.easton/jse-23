package ru.nlmk;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ToDo {

    public static final Logger logger = Logger.getLogger(ToDo.class.getName());

    public List<List<Description>> getList(List<Object> objects) {
        List<List<Description>> result = new ArrayList<>();
        checkClass(objects);
        for(Object object: objects){
            Class clazz = object.getClass();
            do {
                List<Description> subResult = new ArrayList<>();
                parseClass(clazz.getDeclaredFields(), subResult, object);
                if (subResult.size() > 0) result.add(subResult);
                clazz = clazz.getSuperclass();
            } while (clazz.getSuperclass() != null);
        }
        return result;
    }

    private void checkClass(List<Object> objects){
        Class clazz = objects.get(0).getClass();
        for (Object object : objects) {
            if (clazz != object.getClass()){
                throw new IllegalArgumentException();
            }
        }
    }

    private void parseClass(Field[] fields, List<Description> subResult, Object object) {
        for (Field field : fields) {
            boolean hasValue = false;
            try {
                field.setAccessible(true);
                if (field.get(object) != null) {
                    hasValue = true;
                }
                subResult.add(new Description(field.getName(), field.getType().toString(), hasValue));
            } catch (IllegalAccessException e) {
                logger.log(Level.ALL,e.getMessage());
            }
        }
    }


}
